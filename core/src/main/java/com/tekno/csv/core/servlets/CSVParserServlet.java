package com.tekno.csv.core.servlets;

import java.io.IOException;
import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.google.gson.Gson;
import com.tekno.csv.core.service.CSVParseService;

@Component(service = Servlet.class, property = { Constants.SERVICE_DESCRIPTION + "= TEST SERVLET",
		"sling.servlet.methods=" + HttpConstants.METHOD_GET, "sling.servlet.methods=" + HttpConstants.METHOD_POST,
		"sling.servlet.paths=/bin/services/searchFromCSV" })
public class CSVParserServlet extends SlingAllMethodsServlet {

	@Reference
	ResourceResolverFactory resolverFactory;

	@Reference
	CSVParseService csvParseService;

	private static final long serialVersionUID = 9101474108840760621L;

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	@Override
	protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException {
		String search = request.getParameter("searchValue");
		response.getWriter().print(new Gson().toJson(csvParseService.parseCSVFile(search)));

	}
}
