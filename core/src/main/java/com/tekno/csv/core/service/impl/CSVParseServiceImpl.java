package com.tekno.csv.core.service.impl;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.dam.api.Asset;
import com.tekno.csv.core.constants.CsvConstants;
import com.tekno.csv.core.models.DataModel;
import com.tekno.csv.core.service.CSVParseService;

@Designate(ocd = CSVParseServiceImpl.Config.class)
@Component(service = CSVParseService.class, immediate = true)
public class CSVParseServiceImpl implements CSVParseService {

	Logger log = LoggerFactory.getLogger(this.getClass());

	private String csvPath;
	private String findWith;
	private String[] expectedResultSet;

	@ObjectClassDefinition(name = "Custom CSV Data Parser")
	public @interface Config {

		@AttributeDefinition(name = "CSV File link", description = "Enter the file path for the CSV file")
		public String csvFilePath() default "/content/dam/rhicustomerfront/customGen.csv";

		@AttributeDefinition(name = "Find With", description = "Key Column Index Number from which data is mapped. note: starts from 0")
		public String findWith() default "0";

		@AttributeDefinition(name = "Result Set", description = "Number(Column Index number) of Column for result set. note: starts from 0")
		public String[] resultSet() default { "0", "1", "2", "3" };

	}

	@Reference
	ResourceResolverFactory resolverFactory;

	@Activate
	public void activate(final Config config) {
		configure(config);
	}

	@Modified
	public void modify(final Config config) {
		configure(config);
	}

	public void configure(final Config config) {
		this.csvPath = config.csvFilePath();
		this.findWith = config.findWith();
		this.expectedResultSet = config.resultSet();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DataModel> parseCSVFile(String search) {
		List<DataModel> dataList = new ArrayList<>();
		Map<String, Object> authInfo = new HashMap<String, Object>();
		authInfo.put(ResourceResolverFactory.SUBSERVICE, CsvConstants.SUB_SERVICE);
		ResourceResolver resolver = null;
		try {
			resolver = resolverFactory.getServiceResourceResolver(authInfo);
		} catch (LoginException e) {
			e.printStackTrace();
		}
		if (resolver != null) {
			Resource resource = resolver.getResource(this.csvPath);
			if (resource != null) {
				Asset csvAsset = resource.adaptTo(Asset.class);
				InputStream csvStream = csvAsset.getOriginal().getStream();

				try {
					String cvsSplitBy = ",";
					InputStream inputStream = csvAsset.getOriginal().getStream();
					List<String> rows = IOUtils.readLines(inputStream, "UTF8");
					Iterator<String> rowItr = rows.iterator();
					if (rowItr.hasNext()) {
						rowItr.next();
					}
					DataModel dataModel = null;

					while (rowItr.hasNext()) {

						String line = rowItr.next().toString();
						String[] csvSplitString = line.split(cvsSplitBy);
						String id = csvSplitString[0];
						String name = csvSplitString[1];
						String contact = csvSplitString[2];
						String email = csvSplitString[3];

						if (findWith.equals("0")) {
							dataModel = new DataModel();
							if (search.equals(id)) {
								addDetails(expectedResultSet, dataModel, id, name, contact, email);
								dataList.add(dataModel);
							}

						} else if (findWith.equals("1")) {
							dataModel = new DataModel();
							if (search.equals(name)) {
								addDetails(expectedResultSet, dataModel, id, name, contact, email);
								dataList.add(dataModel);
							}
						} else if (findWith.equals("2")) {
							dataModel = new DataModel();
							if (search.equals(contact)) {
								addDetails(expectedResultSet, dataModel, id, name, contact, email);
								dataList.add(dataModel);
							}
						} else {
							// findWith.equals("3")
							dataModel = new DataModel();
							if (search.equals(email)) {
								addDetails(expectedResultSet, dataModel, id, name, contact, email);
								dataList.add(dataModel);
							}
						}
					}

					return dataList;
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} finally {
					try {
						csvStream.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
					resolver.close();
				}
			}
		}
		return null;
	}

	private void addDetails(String[] expectedResultSet, DataModel dataModel, String id, String name, String contact,
			String email) {
		for (String number : expectedResultSet) {
			if (Integer.parseInt(number) == 0) {
				dataModel.setId(id);
			}
			if (Integer.parseInt(number) == 1) {
				dataModel.setName(name);
			}
			if (Integer.parseInt(number) == 2) {
				dataModel.setContact(contact);
			}
			if (Integer.parseInt(number) == 3) {
				dataModel.setEmail(email);
			}
		}
	}

}