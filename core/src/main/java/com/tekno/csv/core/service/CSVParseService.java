package com.tekno.csv.core.service;

import java.util.List;
import com.tekno.csv.core.models.DataModel;

public interface CSVParseService {

	public List<DataModel> parseCSVFile(String search);

}
